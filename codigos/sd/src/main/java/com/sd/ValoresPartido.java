/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sd;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
 import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
 import org.apache.hadoop.io.Text;
 import org.apache.hadoop.mapreduce.Job;
 import org.apache.hadoop.mapreduce.Mapper;
 import org.apache.hadoop.mapreduce.Reducer;
 import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
 import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
 import org.apache.hadoop.util.GenericOptionsParser;
/**
 *
 * @author afonso
 */
public class ValoresPartido {
    public static class TokenizerMapper
        extends Mapper<Object, Text, Text, FloatWritable>{
 
     private final static FloatWritable one = new FloatWritable(1);
     private Text word = new Text();
 
     public void map(Object key, Text value, Context context
                     ) throws IOException, InterruptedException {
         String[] data = value.toString().split("[\";]+");
         word.set(data[4]);
         String numero = data[10].replace(',', '.');
         float gasto;
         try{
            gasto = Float.parseFloat(numero);
            one.set(gasto);
            context.write(word,one);
         }catch(NullPointerException|NumberFormatException  ex){
             one.set(1);
             word.set("000000-Fail");
             context.write(word,one);
         }
         
     }
   }
 
   public static class IntSumReducer
        extends Reducer<Text,FloatWritable,Text,FloatWritable> {
     private FloatWritable result = new FloatWritable();
 
     public void reduce(Text key, Iterable<FloatWritable> values,
                        Context context
                        ) throws IOException, InterruptedException {
       float sum = 0;
         for (FloatWritable val : values) {
           sum += val.get();
         }
         result.set(sum);
         context.write(key, result);
     }
   }
 
   public static void main(String[] args) throws Exception {
     Configuration conf = new Configuration();
     String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
     if (otherArgs.length != 2) {
       System.err.println("Usage: wordcount <in> <out>");
       System.exit(2);
     }
     Job job = new Job(conf, "word count");
     job.setJarByClass(ValoresPartido.class);
     job.setMapperClass(TokenizerMapper.class);
     job.setCombinerClass(IntSumReducer.class);
     job.setReducerClass(IntSumReducer.class);
     job.setOutputKeyClass(Text.class);
     job.setOutputValueClass(FloatWritable.class);
     FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
     FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
     System.exit(job.waitForCompletion(true) ? 0 : 1);
   }
}
